import json
from datetime import datetime

import cv2
import numpy as np
import requests
import os

URL = "http://192.168.43.1:8080/shot.jpg"
WIDTH = 331
HEIGHT = 331
DIM = (WIDTH, HEIGHT)
FONT = cv2.FONT_HERSHEY_SIMPLEX
BATCH_SIZE = 25
ALERT_TEST = 0.6
CAMERA_ID_TEST = 1

os.environ["OPENCV_FFMPEG_CAPTURE_OPTIONS"] = "rtsp_transport;udp"

RTSP_URL = "rtsp://192.168.1.59:5554/camera"


# Day data len firebwase
def put_to_firebase(time, alert, camera_id):
    data = {
        "time": time,
        "alert": alert,
        "camera_id": camera_id
    }
    requests.put('https://delta-entry-160518.firebaseio.com/collectors.json', data=json.dumps(data))


if __name__ == '__main__':

    count = 0
    images = []
    batch_index = 0
    vcap = cv2.VideoCapture(RTSP_URL, cv2.CAP_FFMPEG)

    while True:
        img_res = requests.get(URL)
        img_arr = np.array(bytearray(img_res.content), dtype=np.uint8)
        img = cv2.resize(cv2.imdecode(img_arr, -1), DIM, interpolation=cv2.INTER_AREA)

        images.append(img)
        if len(images) == BATCH_SIZE:
            # Dem so batch
            batch_index += 1
            print(batch_index)

            # Test day data theo batch_size
            timestamp = datetime.now().strftime("%4Y-%2m-%2d %2H:%2M:%2S")
            put_to_firebase(time=timestamp, alert=ALERT_TEST, camera_id=CAMERA_ID_TEST)

            del images[:]

        cv2.putText(img, 'Antimatlab', (0, HEIGHT - 3), FONT, 1, (255, 255, 255), 2, cv2.LINE_AA)
        cv2.imshow('AndroidCam', img)
        if cv2.waitKey(1) == 27:
            break
