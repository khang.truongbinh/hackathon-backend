import easygui
import sys
from easygui import msgbox, choicebox, ccbox, multenterbox, boolbox


class GUI:
    def __init__(self):
        self.title = "Antimatlab"

    def ask_continue(self, msg):
        return str(ccbox(msg, self.title))

    def execute(self):
        cmd = msgbox("Thang 1"
            , title=self.title, ok_button="Tiếp tục")
        if cmd == "Tiếp tục":
            choices = ["Train", "Predict"]
            choice = choicebox("Chọn chế độ", self.title, choices)
            print("choice: ", choice)
        else:
            sys.exit(0)  # user chose Cancel

        if choice == "Train":
            file_path = easygui.fileopenbox(default="*.txt", multiple=True)
            print(file_path)
            if file_path is None:
                return self.ask_continue("Không có file, bạn có muốn tiếp tục?")
            for file_ele in file_path:
                if file_ele is None or file_ele[-3:] != "txt":
                    return self.ask_continue("Sai định dạng, bạn có muốn tiếp tục?")
            try:
                flag = self.train_model(file_path)
            except:
                flag = 0
            print("flag: " + str(flag))
            if flag == 1:
                msg = "Training thành công, model lưu tại xxx_lstm.h5 và xxx_lstm.json"
            else:
                msg = "Training thât bại. Vui lòng thực hiện lại"
        elif choice == "Predict":
            try:
                result = self.test_mode()
                msg = result
            except:
                msg = "Chưa đủ dữ kiện dự đoán. Vui lòng nhập liệu lại"

        else:
            return "out of choosing window"
        return str(ccbox(msg, self.title, choices=["Tiếp tục", "Hủy"],
                         default_choice='Tiếp tục', cancel_choice='Hủy'))

    def train_model(self, data_files):
        msg = "Nhập thông số để training"
        title = self.title
        epoch = self.select_epochs()
        optimizer = self.select_optimizer()
        flag = 0
        print(epoch, optimizer)
        try:
            flag = lstm.train_api(data_files=data_files, epochs=int(epoch), optimizer=optimizer)
        except:
            flag = 0
            print("loi training")
        return flag

    def select_epochs(self):
        choices = ["1000", "2000", "5000", "10000", "20000", "200000"]
        choice = choicebox("Chọn số epochs", self.title, choices)
        return int(choice)

    def select_optimizer(self):
        choices = ["adam", "sgd"]
        choice = choicebox("Chọn optimizer", self.title, choices)
        return choice

    def test_mode(self):
        msg = "Điền tham số để dự đoán"
        title = self.title
        fieldNames = ["% hoàn thành dự án hiện tại", "% hoàn thành dự án tháng trước",
                      "% hoàn thành dự án 2 tháng trước",
                      "Tổng chi phí bỏ ra hiện tại", "Tổng chi phí bỏ ra tháng trước",
                      "Tổng chi phí bỏ ra 2 tháng trước"]
        fieldValues = []  # we start with blanks for the values
        fieldValues = multenterbox(msg, title, fieldNames)

        # make sure that none of the fields was left blank
        while 1:
            if fieldValues is None: break
            errmsg = ""
            for i in range(len(fieldNames)):
                if fieldValues[i].strip() != "" and fieldNames[i][0] == "x" and (
                        float(fieldValues[i]) >= 1.0 or float(fieldValues[i]) <= 0.0):
                    errmsg += ('"%s" phải lớn hơn 0 và nhỏ hơn 1.\n\n' % fieldNames[i])
                if fieldValues[i].strip() == "":
                    errmsg += ('"%s" không được bỏ trống.\n\n' % fieldNames[i])
            if errmsg == "":
                break  # no problems found
            fieldValues = multenterbox(errmsg, title, fieldNames, fieldValues)
        test_result = test_lstm.test_lstm_api([float(fieldValues[2]), float(fieldValues[1]), float(fieldValues[0])],
                                              [float(fieldValues[5]), float(fieldValues[4]), float(fieldValues[3])])
        print(test_result)
        return "Dự đoán chi phí cuối cùng: " + str(test_result[0])

    def run(self):
        while 1:
            flag = self.execute()
            print("flag:", flag)
            if flag == "True":
                continue
            else:
                break
        sys.exit(0)


if __name__ == '__main__':
    gui = GUI()
    gui.run()
