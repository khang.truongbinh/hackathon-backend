from pyfcm import FCMNotification

API_KEY = "AIzaSyBi-FwrNCZefPqNtz7SjLe9k66i8ndwQcY"
TOKEN_FILE = 'tokens.txt'
MESSAGE_TITLE = "Sexual Harassment Warning!"
MESSAGE_BODY = "Location: Elevator 1, Warning: 0.91"
# # Your api-key can be gotten from:  https://console.firebase.google.com/project/<project-name>/settings/cloudmessaging
#
# # Send to multiple devices by passing a list of ids.
# registration_ids = [line.rstrip('\n') for line in open('tokens.txt', 'r')]
# print(registration_ids)
# # registration_ids = ["etU8nPfshpE:APA91bFeQhrM4VgUYZGdDtYb9bpEgIiPHUbnCB0__WhuDjJydkx1oEZaBwjMjd2EMRIjxF0HOe_7b3uH0oHl2RvVGikyLYEXQHfNZyXrIfiPXNtGJGdZSQC7xFNBh3sH44EEHF0-JWeS"]
# message_title = "Sexual Harassment Warning!"
# message_body = "Location: Elevator 1, Warning: 0.91"
# result = push_service.notify_multiple_devices(registration_ids=registration_ids, message_title=message_title,
#                                               message_body=message_body,
#                                               voice=True)
# print(result)


def send_message_to_apps(api_key, token_file, message_title, message_body, sound):
    """
    :param api_key:
    :param token_file:
    :param message_title:
    :param message_body:
    :return: None
    """
    # Gui thong bao len cho app
    registration_ids = [line.rstrip('\n') for line in open(token_file, 'r')]
    push_service = FCMNotification(api_key=api_key)
    result = push_service.notify_multiple_devices(registration_ids=registration_ids, message_title=message_title,
                                                  message_body=message_body, sound=sound
                                                  )
    print(result)

