from flask import Flask
from flask import request
import json
app = Flask(__name__)

point = 1
@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/post', methods=['POST', 'GET'])
def post():
    if request.method == 'POST':
        global point
        point = request.form.get('point')
        print(point)
        return 'JSON posted'

@app.route('/get', methods=['POST', 'GET'])
def get():
    if request.method == 'GET':
        return json.dumps({
            "point": point})


if __name__ == '__main__':
    app.run()
