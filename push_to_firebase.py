import requests


# Just a test
#DATA = '{ "time": "2019-07-13 12:34", "alert": 0.4, "camera_id": 1 }'
#response = requests.put('https://delta-entry-160518.firebaseio.com/collectors.json', data=DATA
#print(response)

# Test json
def put_to_firebase(time, alert, camera_id):
    data = {
            "time": time,
            "alert": alert,
            "camera_id": camera_id
            }
    requests.put('https://delta-entry-160518.firebaseio.com/collectors.json', data=DATA)
    return requests.status_code
