# from keras.applications.inception_v3 import InceptionV3
import json
import os
import time
from datetime import datetime
from threading import Thread

import cv2
import numpy as np
import requests
from imutils.video import FPS
from keras.applications.mobilenet import MobileNet
from keras.models import load_model

from config.config import WEIGHT_PATH

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
URL = "http://192.168.43.1:8080/shot.jpg"
ALERT_TEST = 0.6
CAMERA_ID_TEST = 1

inception_v3 = MobileNet(input_shape=None, include_top=True, weights='imagenet', input_tensor=None, pooling=None,
                         classes=1000)
# inception_v3.summary()
inception_v3.layers.pop()
inception_v3.outputs = [inception_v3.layers[-1].output]
inception_v3.layers[-1].outbound_nodes = []
classifier = load_model(WEIGHT_PATH)


def add_predict(frame, predict):
    action_mapping = {0: 'Abnormal', 1: 'Normal'}
    color = (0, 255, 0)
    if predict == 0:
        color = (0, 0, 255)
    # height, width, _ = frame.shape
    cv2.rectangle(frame, (0, 0), (224, 224), color, 2)
    cv2.putText(frame, action_mapping[predict], (10, 15), cv2.FONT_HERSHEY_SIMPLEX, 1, color, thickness=2, lineType=2)


FRAME_INTERVAL = 64  # Number of frames after which to run face detection
fps_display_interval = 5  # frames
frame_rate = 0

# cap = cv2.VideoCapture('D:/antimatlab/sexual video/videoplayback (9).mp4')
# _, frame = cap.read()
img = requests.get(URL)
frame = np.array(bytearray(img.content), dtype=np.uint8)

start_time = time.time()

segment = []


# Day data len firebase
def put_to_firebase(time, alert, camera_id):
    data = {
        "time": time,
        "alert": alert,
        "camera_id": camera_id
    }
    requests.put('https://delta-entry-160518.firebaseio.com/collectors.json', data=json.dumps(data))


# height, width, layers = frame.shape
# fourcc = cv2.VideoWriter_fourcc(*'MPEG')
# output_video = cv2.VideoWriter('D:/antimatlab/sexual video/videoplayback (9)_out.mp4', fourcc, 20.0,
#                                (width, height))
predict = None
ret = True
frame_count = 0
cv2.namedWindow('AndroidCam', cv2.WINDOW_AUTOSIZE)
# fps = cap.get(cv2.CAP_PROP_FPS)
# print("fps: ", fps)
fps = FPS().start()
while 1:
    # frame = cap.read()
    img_res = requests.get(URL)
    img_arr = np.array(bytearray(img_res.content), dtype=np.uint8)
    try:
        # image = cv2.resize(img_arr, (224, 224), interpolation=cv2.INTER_AREA)
        image = cv2.resize(cv2.imdecode(img_arr, -1), (224, 224), interpolation=cv2.INTER_AREA)
        segment.append(image)
        frame_count += 1
    except Exception as e:
        continue

    if len(segment) == FRAME_INTERVAL:
        print(1)

        #
        # def task():
        #     features = inception_v3.predict(np.array(segment))
        #     global result
        #     tmp = classifier.predict(np.array(features).reshape(1, FRAME_INTERVAL, 1000))
        #     result = tmp
        #     print(tmp)
        #     print(result)
        features = inception_v3.predict(np.array(segment))
        result = classifier.predict(np.array(features).reshape(1, FRAME_INTERVAL, 1000))

    # thread = Thread(target=task)
    # print(result)
        del segment[:]
        predict = np.argmax(result[0], axis=0)
        if predict is not None and frame_count % FRAME_INTERVAL < 6:
            add_predict(image, predict)
            timestamp = datetime.now().strftime("%4Y-%2m-%2d %2H:%2M:%2S")
            put_to_firebase(time=timestamp, alert=float(result[0][0]), camera_id=CAMERA_ID_TEST)

    cv2.imshow('AndroidCam', image)
    if cv2.waitKey(1) == 27:
        break
# fps.update()
# cv2.waitKey(1000/fps.fps())
# fps.stop()
# cv2.waitKey(int(1000 / (fps.fps() + 0.001)))
# output_video.write(frame)

# When everything is done, release the capture
# cap.release()
# output_video.release()
cv2.destroyAllWindows()
