import json
# import os
# import time

import cv2
import numpy as np
import requests
import os

IP_CAMERA_URL = "http://192.168.43.1:8080/shot.jpg"
FIRE_BASE_URL = 'https://delta-entry-160518.firebaseio.com/collectors.json'
LOCAL_URL = ''
WIDTH = 224
HEIGHT = 224
FRAME_INTERVAL = 64
DEFAULT_PREDICT_VALUES = 0

os.environ["OPENCV_FFMPEG_CAPTURE_OPTIONS"] = "rtsp_transport;udp"
RTSP_URL = "rtsp://192.168.1.59:5554/camera"


def put_to_fire_base(time_stamp, alert, camera_id):
    data = {
        "time": time_stamp,
        "alert": alert,
        "camera_id": camera_id
    }
    requests.put(FIRE_BASE_URL, data=json.dumps(data))


def add_predict(frame, predict):
    action_mapping = {0: 'Abnormal', 1: 'Normal'}
    color = (0, 255, 0)
    if predict == 0 and count%15 < 6:
        color = (0, 0, 255)
        # height, width, _ = frame.shape
        cv2.rectangle(frame, (0, 0), (224, 224), color, 2)
        cv2.putText(frame, action_mapping[predict], (10, 15), cv2.FONT_HERSHEY_SIMPLEX, 1, color, thickness=2,
                    lineType=2)


def push_image_to_detect_server(url, data):
    requests.put(url, data=data)


if __name__ == '__main__':
    count = 0
    VIDEO_FILE = 'video.mp4'
    segment = []
    vcap = cv2.VideoCapture(VIDEO_FILE)
    global fps
    fps = vcap.get(cv2.CAP_PROP_FPS)
    while 1:
        ret, frame = vcap.read()
        # resources = requests.get(IP_CAMERA_URL)
        # frame_array = np.array(bytearray(resources.content), dtype=np.uint8)

        # image = cv2.resize(cv2.imdecode(frame, -1), (WIDTH, HEIGHT), interpolation=cv2.INTER_AREA)
        frame = cv2.resize(frame, (224, 224), interpolation = cv2.INTER_AREA)

        # point = requests.get(FIRE_BASE_URL).json()['alert'] < 0.5
        point = requests.get('http://localhost:5000/get').json()['point']
        # print(point)
        add_predict(frame, int(point), count)
        count += 1
        cv2.imshow('Antimatlab', frame)
        cv2.waitKey(int(1000/fps))
        if cv2.waitKey(1) == 27:
             break
