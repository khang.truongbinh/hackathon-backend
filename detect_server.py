import json
import os
import time
from datetime import datetime
from threading import Thread

import cv2
import numpy as np
import requests
# from imutils.video import FPS
from keras.applications.mobilenet import MobileNet
from keras.models import load_model

from config.config import WEIGHT_PATH

global point
point = 1
from pyfcm import FCMNotification

API_KEY = "AIzaSyBi-FwrNCZefPqNtz7SjLe9k66i8ndwQcY"
TOKEN_FILE = 'tokens.txt'
MESSAGE_TITLE = "Sexual Harassment Warning!"


def send_message_to_apps(api_key, token_file, message_title, message_body, sound):
    """
    :param api_key:
    :param token_file:
    :param message_title:
    :param message_body:
    :return: None
    """
    # Gui thong bao len cho app
    registration_ids = [line.rstrip('\n') for line in open(token_file, 'r')]
    push_service = FCMNotification(api_key=api_key)
    result = push_service.notify_multiple_devices(registration_ids=registration_ids, message_title=message_title,
                                                  message_body=message_body, sound=sound
                                                  )
    print(result)


os.environ["OPENCV_FFMPEG_CAPTURE_OPTIONS"] = "rtsp_transport;udp"
RTSP_URL = "rtsp://192.168.1.59:5554/camera"



#
# def put_to_fire_base(time_stamp, alert, camera_id):
#     data = {
#         "time": time_stamp,
#         "alert": alert,
#         "camera_id": camera_id
#     }
#     requests.put(FIRE_BASE_URL, data=json.dumps(data))


def add_predict(frame, predict):
    action_mapping = {0: 'Abnormal', 1: 'Normal'}
    color = (0, 255, 0)
    if predict == 0:
        color = (0, 0, 255)
    # height, width, _ = frame.shape
    cv2.rectangle(frame, (0, 0), (224, 224), color, 2)
    cv2.putText(frame, action_mapping[predict], (10, 15), cv2.FONT_HERSHEY_SIMPLEX, 1, color, thickness=2, lineType=2)


if __name__ == '__main__':

    # FIRE_BASE_URL = 'https://delta-entry-160518.firebaseio.com/collectors.json'
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    URL = "http://192.168.43.1:8080/shot.jpg"
    ALERT_TEST = 0.6
    CAMERA_ID_TEST = 1
    FRAME_INTERVAL = 64  # Number of frames after which to run face detection
    VIDEO_FILE = 'video.mp4'
    vcap = cv2.VideoCapture(VIDEO_FILE)
    inception_v3 = MobileNet(input_shape=None, include_top=True, weights='imagenet', input_tensor=None, pooling=None,
                             classes=1000)
    # inception_v3.summary()
    inception_v3.layers.pop()
    inception_v3.outputs = [inception_v3.layers[-1].output]
    inception_v3.layers[-1].outbound_nodes = []
    classifier = load_model(WEIGHT_PATH)
    segment = []
    frame_count = 0
    count = 0
    global fps
    fps = vcap.get(cv2.CAP_PROP_FPS)
    # vcap = cv2.VideoCapture(RTSP_URL, cv2.CAP_FFMPEG)


    while 1:
        count += 1
        print("count= ", count)
        # frame = cap.read()
        # img_res = requests.get(URL)
        ret, frame = vcap.read()
        # img_arr = np.array(bytearray(frame.content), dtype=np.uint8)
        # image = cv2.resize(img_arr, (224, 224), interpolation=cv2.INTER_AREA)
        # img_arr = cv2.imdecode(frame, -1)
        height, width, _ = frame.shape
        if height == 0 and width == 0:
            continue
        image = cv2.resize(frame, (224, 224), interpolation=cv2.INTER_AREA)
        frame_count += 1

        n_frame = len(segment)
        if n_frame < FRAME_INTERVAL:
            segment.append(image)
            segment.append(image)
            segment.append(image)
        else:
            segment.pop(0)
            segment.pop(0)
            segment.pop(0)
            segment.append(image)
            segment.append(image)
            segment.append(image)
        if n_frame >= FRAME_INTERVAL:
            print('|||||||||||||||||||||||||||||||')
            features = inception_v3.predict(np.array(segment[-FRAME_INTERVAL:]))
            result = classifier.predict(np.array(features).reshape(1, FRAME_INTERVAL, 1000))
            point = result[0][0]
            # point = float(requests.get(FIRE_BASE_URL).json()['alert']) < 0.5
            point = result[0][0] < 0.75
            if result[0][0] > 0.75:
                confidence = round(result[0][0], 2)
                print(confidence)
                msg_body = 'Location: Elevator 1, Warning: ' + '{:.2f}'.format(confidence)
                print(msg_body)
                send_message_to_apps(API_KEY, TOKEN_FILE, MESSAGE_TITLE, msg_body, sound=True)
                requests.post('http://localhost:5000/post', data={'point': int(point)})
            #
            # def task():
            #     features = inception_v3.predict(np.array(segment))
            #     global result
            #     tmp = classifier.predict(np.array(features).reshape(1, FRAME_INTERVAL, 1000))
            #     result = tmp
            #     print(tmp)
            #     print(result)

            # thread = Thread(target=task):wq

            # print(result)
            # del segment[:]
            predict = np.argmax(result[0], axis=0)
            # if predict is not None and frame_count % FRAME_INTERVAL < 6:
            if predict == 0:
                timestamp = datetime.now().strftime("%4Y-%2m-%2d %2H:%2M:%2S")
                # put_to_fire_base(time_stamp=timestamp, alert=float(result[0][0]), camera_id=CAMERA_ID_TEST)
        if cv2.waitKey(1) == 27:
            break
        cv2.waitKey(int(1000/fps))
